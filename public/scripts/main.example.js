/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();
console.log(app.inputPassangers.value);
const btn_load = app.loadButton;
btn_load.addEventListener("click", function (e) {
   e.preventDefault();
   console.log(app.inputDate.value);
   console.log(app.inputTime.value);
   console.log(app.dateTime);
   app.carContainerElement.innerHTML = "";
   app.loadData(app.inputPassangers.value).then(app.run)


})
// app.init().then(app.run);

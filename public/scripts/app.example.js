class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.inputDriverAvailability = document.getElementById(
      "inputDriverAvailability"
    );
    this.inputDate = document.getElementById("inputDate");
    this.inputTime = document.getElementById("inputTime");
    this.inputPassangers = document.getElementById("inputPassangers");
    this.dateTime = (`${this.inputDate.value}T${this.inputTime.value}`)
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.addEventListener("click", (e) => {
      e.preventDefault();
      let child = this.carContainerElement.firstElementChild;

      while (child) {
        child.remove();
        child = this.carContainerElement.firstElementChild;
      }
    });
    this.loadButton.onclick = this.run;
  }

  async loadData(Human) {
    this.inputDate = document.getElementById("inputDate").value;
    this.inputTime = document.getElementById("inputTime").value;

    let dateTime = new Date(`${this.inputDate} ${this.inputTime}`)
    const newTime = dateTime.getTime()
    console.log(newTime);
    console.log(dateTime);
    let cars = await Binar.listCars((item) => {
      const newTimeJson = new Date(item.availableAt);
      const valueTime = newTimeJson < newTime; 
      const value = item.capacity >= Number(Human);
      return value && valueTime


    });
      
    Car.init(cars);
  }

  run = () => {
    const node = document.createElement("div");
    node.className = "row";
    this.carContainerElement.className = "container";
    const baru = document.createElement("div");
    baru.className = "container";
    node.appendChild(baru);

    const baris = document.createElement("div");
    baris.className = "row";
    this.carContainerElement.appendChild(baris);

    Car.list.forEach((car) => {
      const col = document.createElement("div");
      col.className = "col-lg-4 col-md-12 col-sm-12"
      col.innerHTML = car.render();
      baris.appendChild(col)
    });
  };

  async load() {
    let cars = await Binar.listCars();

    Car.init(cars);
  }

  clear = (e) => {
    e.preventDefault();
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
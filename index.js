const http = require('http');
const FS = require('fs');
const PATH = require('path');
const express = require('express');
const res = require("express/lib/response");
const APP = express();
const PORT = 8000;

APP.use(express.static(__dirname + "/public"))

APP.get('/', function(req, res) {
    res.sendFile(PATH.join(__dirname + "/public/landing.html"))
})

APP.get('/cariMobil', function(req, res) {
    res.sendFile(PATH.join(__dirname + "/public/cari.html"))
})

APP.listen(PORT, () => {
    console.log(`Berjalan di port ${PORT}`);
})